console.warn("For Loop function")
let newArray = []

const array = [1,2,3,4,5];
array.forEach((value, index) => newArray[index] = value + 1);
console.warn("array", array, newArray)

console.warn("\nMap function")

const mappedArray = array.map((value, index) => value + 1);
console.warn("mappedArray", array, mappedArray)

console.warn("\nReduce function")
const reducedArray = array.reduce((accumulator, currentValue) => {
    if (currentValue > 2) accumulator += currentValue
    return accumulator
}, 0)
console.warn("reducedArray", reducedArray, array)

const values = [{number: 1}, {number: 2}, {number: 3}];
const reducedValues = values.reduce((acc, cur) => {
    acc[cur.number] = cur
    return acc
}, {})
console.warn("reducedValues", reducedValues, reducedValues[1])

console.warn("\nFilter Function")
const filteredArray = values.filter((value, index) => value.number === 1)
console.warn("filteredArray", filteredArray)