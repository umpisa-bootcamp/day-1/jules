const assert = require('assert')
const { sum, subtract } = require('../src/operations')

describe('Summation Function', () => {
    it('should add 5 and 3 to 8', () => {
        assert.equal(sum(5, 3), 8)
        console.warn("ty")
    })
    it('should not add 5 and 3 to 7', () => {
        assert.equal(sum(5, 3) === 7, false)
    })
    it('should not add null and null', () => {
        assert.equal(sum(null, null), null)
    })
})

describe('Subtraction Function', () => {

})