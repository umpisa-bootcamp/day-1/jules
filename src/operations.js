const sum = (x, y) => {
    if (x === null && y === null) {
        return null
    }
    return x + y
}

const subtract = (x, y) => {
    return x - y;
}

const multiply = (x, y) => {
    return x * y;
}

const divide = (x, y) => {
    return x / y;
}

module.exports = {
    sum,
    subtract,

}