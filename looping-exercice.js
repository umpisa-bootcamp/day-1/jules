const newArray = [
    {id: 1, name: 'test1', password: '123'},
    {id: 2, name: 'test2', password: '123'},
    {id: 3, name: 'test3', password: '123'}]

console.warn("Part 1")
const ids = newArray.map((value) => {
    console.warn("value", value)
    const newValue = value.id
    return newValue
})
console.warn("ids", ids)
console.warn("\n")

console.warn("Part 2")
const filtered = newArray.filter(value => value.id === 2)
console.warn("filtered", filtered)
console.warn("\n")

console.warn("Part 3")
const transformed = newArray.reduce((acc, cur) => {
    acc[cur.id] = cur
    return acc
}, {})
console.warn("transformed", transformed, transformed[2])